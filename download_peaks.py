import json
import requests


def download_peaks(file='peak_data.json'):
    '''
    Retrieves all mountain peaks from OpenStreetMap using Overpass API
    Observe the usage policy of the mains Overpass API instance.
    The query below currently returns more than 113 MB.
    '''
    overpass_url = 'https://overpass-api.de/api/interpreter'
    overpass_query = '''[timeout:600][out:json];
                        (node["natural"="peak"]["ele"];
                        node["natural"="volcano"]["ele"];);
                        out center;'''
    # For testing purposes: Add 'area["ISO3166-2"="US-HI"][admin_level=4];'
    response = requests.get(overpass_url,
                            params={'data': overpass_query})
    data = response.json()

    with open(file, 'w', encoding='utf-8') as data_file:
        json.dump(data, data_file, indent=4, ensure_ascii=False)
    return


if __name__ == '__main__':
    download_peaks()
