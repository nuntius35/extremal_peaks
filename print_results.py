import json
import argparse
from timeit import default_timer as timer


def main():
    print('Printing the results.')
    print('Reading data............................', end='', flush=True)
    start = timer()
    with open('peak_data.json', 'r', encoding='utf-8') as data_file:
        data = json.load(data_file)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))

    print('Reading extremals.......................', end='')
    start = timer()
    ex = []
    with open('extremals.mat', 'r', encoding='utf-8') as extremals:
        for i in extremals:
            ex.append(int(i)-1)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))

    print('Stripping geojson.......................', end='', flush=True)
    start = timer()
    geojson = {"type": "FeatureCollection",
               "features": [{"type": "Feature",
                             "geometry": {"type": "Point",
                                          "coordinates": [peak['lon'],
                                                          peak['lat']]},
                                          "properties": {key: value
                                                         for key, value in peak['tags'].items() if key in ['name', 'ele', 'name:en', 'wikipedia']}
                                          }
                            for peak in [data['elements'][i] for i in ex]
                            ]
               }
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
    print('Writing geojson.........................', end='', flush=True)
    start = timer()
    with open('extremals.geojson', 'w', encoding='utf-8') as data_file:
        json.dump(geojson, data_file, indent=4, ensure_ascii=False)
        data_file.write('\n')
    end = timer()
    print('[{:7.3f} s]'.format(end-start))


if __name__ == '__main__':
    main()
