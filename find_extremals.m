clear all;
fprintf('[Octave] Find extremal peaks. \nThis may take several minutes.\n');

function y = hav(x)
  % The haversine function hav(θ) = sin²(θ/2) = (1 - cos(θ))/2.
  y = sin(x/2).^2;
endfunction

function d = dominates(peak1, peak2)
  % Returns true, if peak1 dominates peak2, false otherwise.
  % Uses the haversine formula to determine the spherical angle
  % between the peaks. Recall the identity cos(hav⁻¹(x)) = 1 - 2x.
  % In vectorized code, one of the arguments can be an N x 3 matrix,
  % the other must be a 1 x 3 vector.
  R = 6371080;
  y = hav(peak2(:,1) - peak1(:,1))...
      + cos(peak1(:,1)).*cos(peak2(:,1)).*hav(peak2(:,2) - peak1(:,2));
  d = ((R+peak1(:,3)).*(1 - 2*y) > (R+peak2(:,3)));
endfunction

function n = filter_nearby(P, peaks)
  % Returns true, if the distance between a peak P and some peaks is less than
  % the radius which is dominated by Mount Everest, false otherwise
  n = logical((P(1)-peaks(:,1)).^2 + (cos(P(1))*(P(2)-peaks(:,2))).^2 < 0.003);
endfunction

fprintf('[Octave] Loading peak data..............');
fflush(stdout);
tic;
  P = dlmread('peak_coords.mat', ',');
  P(:,1:2) = deg2rad(P(:,1:2));
  [P,I] = sortrows(P,-3);
t = toc;
fprintf('[%7.3f s]\n', t);
fflush(stdout);

% In the first pass we iterate over peaks from highest to lowest
% For each peak we eliminate all smaller peaks which are dominated.
% Once a peak is found to be nonextremal, it is ignored in the
% first pass.
fprintf('[Octave] First pass (of two)............');
fflush(stdout);
tic;
  N = length(P);
  dom = ones(N,1);
  for i=1:N-1
    if dom(i)
      peak1 = P(i,:);
      mask = logical(dom.*(1:N>i)');
      nearby = filter_nearby(peak1,P(mask,:));
      mask(mask) =  mask(mask).*nearby;
      dom(mask) = dom(mask) .* (1 - dominates(peak1, P(mask,:)));
    endif
  endfor
t = toc;
fprintf('[%7.3f s]\n',t);
fflush(stdout);

% The first pass produces a good list of candidates for extremal points.
% However, it might (and it does) happen, that such a candidate is
% dominated by another nonextremal peak. Therefore, in the second pass
% we iterate over all candidates resulting from the first pass and check,
% whether they are dominated by a higher peak.
fprintf('[Octave] Second pass....................');
fflush(stdout);
tic;
  ind = find(dom);
  K = length(ind);
  for i=2:K
    j = ind(i);
    peak2 = P(j,:);
    nearby = filter_nearby(peak2, P(1:j-1,:));
    dom(j) = dom(j)*(1 - logical(sum(dominates(P(1:j-1,:)(nearby,:), peak2))));
  endfor
t = toc;
fprintf('[%7.3f s]\n', t);
fflush(stdout);

fprintf('[Octave] Writing results................');
fflush(stdout);
tic;
  dlmwrite('extremals.mat', I(find(dom)));
t = toc;
fprintf('[%7.3f s]\n', t);
fflush(stdout);
