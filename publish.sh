#!/bin/bash
echo "var peaks = " | cat - extremals.geojson > public/res/extremals-geojson.js
sed -i "s/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}/$(date '+%Y-%m-%d')/g" public/index.html
