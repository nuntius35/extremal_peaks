# Extremal peaks

## Introduction
In topography there are the well known concepts of [isolation](https://en.wikipedia.org/wiki/Topographic_isolation) and [prominence](https://en.wikipedia.org/wiki/Topographic_prominence) to characterize the importance of a mountain peak.
For example, the [Großglockner (3798 m)](https://en.wikipedia.org/wiki/Gro%C3%9Fglockner) has a prominence of 2423 m.
This means that any path leading from Glockner to [Mont Blanc (4810 m)](https://en.wikipedia.org/wiki/Mont_Blanc) has to descend at least to the elevation of [Brenner Pass (1375 m)](https://en.wikipedia.org/wiki/Brenner_Pass).
Moreover, the nearest mountain higher than the Glockner is the [Gran Zebrù (3851 m)](https://en.wikipedia.org/wiki/Gran_Zebr%C3%B9) in the Ortler Alps at a distance of 175 km.

However, people have long suspected that the earth is not flat but [spherical](https://en.wikipedia.org/wiki/Spherical_Earth).
It is therefore natural to consider the relations between mountain peaks in three-dimensional space.
We say that a peak is an **extremal point** if the tangent plane attached to the peak does not intersect any other point on earth.
This little project aims to provide software to identify the extremal mountain peaks.

## Software
This repository contains some scripts written in Python and an Octave file.

* `prepare_peaks.py`
This script downloads the data and cleans up some peaks which do not have proper elevation data in OpenStreetMap.
The output of this script are the files `peak_data.json` containing the cleaned data from OpenStreetMap and `peak_coords.mat` containing only the coordinates and elevations of the peaks.
* `download_peaks.py` and `sanitize_ele.py`
Helper scripts called by `prepare_peaks.py` above.
* `find_extremals.m`
This octave program reads the file `peak_coords.mat` and computes all extremal points in this file.
The result is written to the file `extremals.mat` containing the indices of the extremal peaks.
* `print_results.py`
This script reads the original data `peak_data.json` and the result of the previous computation `extremals.mat`.
It prints the extremal peaks in geojson format to the file `extremals.geojson`.

## Requirements

I am using

* [GNU Octave 5.1.0](https://www.gnu.org/software/octave/)
* and [Python 3.7.4](https://www.python.org/).

On a Linux machine with the above software installed you can simply execute `make`.
This will download all peaks from [OpenStreetMap](https://www.openstreetmap.org/) via [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API), find all extremal peaks, write the result to the file `extremals.geojson` and update the map in the `public` directory.
Also, the raw data from Overpass is saved to disc, so that you don't have to query Overpass every time you play around with the data.

You need 200 MB of free disc space and the processing can take several minutes.

Please observe the usage policy of the main [Overpass API](https://overpass-api.de/): Do less than 10000 queries per day and download less than 5 GB data per day.
The query used to obtain all the peaks returns more than 120 MB!

## Interactive map
The results are visualized on a [map](https://nuntius35.gitlab.io/extremal_peaks/).

---
![screenshot](screenshot.png)

---

[Homepage of Andreas Geyer-Schulz](https://nuntius35.gitlab.io)
