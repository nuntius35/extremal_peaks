all: page

data:
	python prepare_peaks.py --force

compute: extremals.mat extremals.geojson

page: public/res/extremals-geojson.js

peak_coords.mat: peak_data.json

peak_data.json:
	python prepare_peaks.py

extremals.mat: peak_coords.mat
	octave find_extremals.m

extremals.geojson: peak_data.json extremals.mat
	python print_results.py

public/res/extremals-geojson.js: extremals.geojson
	./publish.sh

cleanall:
	rm -f extremals.geojson
	rm -f peak_data.json
	rm -f extremals.mat
	rm -f peak_coords.mat

.PHONY: cleanall
