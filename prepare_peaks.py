import json
import argparse
import os.path
from timeit import default_timer as timer
from datetime import datetime
from download_peaks import download_peaks
from sanitize_ele import sanitize_ele


def main():
    parser = argparse.ArgumentParser(
        description='Preparing the peak data.')
    parser.add_argument('-f',
                        '--force',
                        action='store_true',
                        help='Force download of data.')
    args = parser.parse_args()

    print('Preparing peak data.')
    file = 'peak_data.json'
    if (not os.path.isfile(file)) or args.force:
        print('Retrieving data from Overpass API.......',
              end='', flush=True)
        start = timer()
        download_peaks(file)
        end = timer()
        print('[{:7.3f} s]'.format(end-start))
    else:
        mtime = datetime.fromtimestamp(os.path.getmtime(file))
        mtime = mtime.strftime('%Y-%m-%d %H:%M:%S')
        print('Using saved data from {0}'.format(mtime))

    print('Cleaning data...........................')
    start = timer()
    skip = []
    with open(file, 'r', encoding='utf-8') as data_file:
        data = json.load(data_file)
    for i, peak in enumerate(data['elements']):
        lat = peak['lat']
        lon = peak['lon']
        try:
            data['elements'][i]['tags']['ele']\
                = sanitize_ele(str(peak['tags']['ele']))
        except ValueError as detail:
            skip.append(i)
            print('Skip peak: ', detail)
    for i in sorted(skip, reverse=True):
        data['elements'].pop(i)
    with open(file, 'w', encoding='utf-8') as data_file:
        json.dump(data, data_file, indent=4, ensure_ascii=False)
        data_file.write('\n')
    end = timer()
    print('Finished cleaning data..................', end='')
    print('[{:7.3f} s]'.format(end-start))

    print('Reading data............................', end='')
    start = timer()
    peak_list = []
    with open(file, 'r', encoding='utf-8') as data_file:
        data = json.load(data_file)
        for i, peak in enumerate(data['elements']):
            lat = peak['lat']
            lon = peak['lon']
            ele = peak['tags']['ele']
            peak_list.append([lat, lon, ele])
    end = timer()
    print('[{:7.3f} s]'.format(end-start))

    print('Writing coordinates to file.............', end='')
    start = timer()
    with open('peak_coords.mat', 'w', encoding='utf-8') as peak_file:
        for peak in peak_list:
            print(peak[0], ',', peak[1], ',', peak[2], file=peak_file)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))


if __name__ == '__main__':
    main()
