import unittest
import re


def sanitize_ele(ele_string):
    '''
    Attempts to make sense of any value of ele in OSM
    :param ele_string: a string as seen in ele=*
    :returns: a float that represents the elevation in meters
    '''
    ele_match = re.search('[0-9]*[\.,]?[0-9]+', ele_string)
    if ele_match is None:
        raise ValueError('[sanitize_ele] No match in ele=' + ele_string)
    else:
        ele_num = ele_match.group(0)
        ele_num = ele_num.replace(',', '.')
        ele_rem = ele_string[ele_match.span()[1]:]
    if not ele_rem:
        ele = float(ele_num)
        if(ele > 8870):
            raise ValueError('[sanitize_ele] No mountain higher than: '
                             + ele_string)
        return ele
    if re.match('\s*[MmмｍМ米]+', ele_rem):
        ele = float(ele_num)
    elif re.match('\s*;.*', ele_rem):
        ele = float(ele_num)
    elif re.match('\s*[Ff]+', ele_rem):
        ele = float(ele_num) * 0.3048
    else:
        raise ValueError('[sanitize_ele] Unknown unit in ele='
                         + ele_string)
    if(ele > 8870):
        raise ValueError('[sanitize_ele] No mountain higher than: '
                         + ele_string)
    return ele


class TestSanitizeFunction(unittest.TestCase):
    def test_default(self):
        self.assertEqual(sanitize_ele('100'), 100)
        self.assertEqual(sanitize_ele('100.34'), 100.34)
        self.assertEqual(sanitize_ele('200,45'), 200.45)

    def test_prefix(self):
        self.assertEqual(sanitize_ele('~100'), 100)

    def test_meters(self):
        self.assertEqual(sanitize_ele('97m'), 97)
        self.assertEqual(sanitize_ele('98 m'), 98)
        self.assertEqual(sanitize_ele('197 meters'), 197)
        self.assertEqual(sanitize_ele('297    mètres'), 297)
        self.assertEqual(sanitize_ele('897M'), 897)
        self.assertEqual(sanitize_ele('797 m ü.d.M.'), 797)
        self.assertEqual(sanitize_ele('4397 m Seehöhe'), 4397)
        self.assertEqual(sanitize_ele('200,45 м'), 200.45)
        self.assertEqual(sanitize_ele('400ｍ '), 400)
        self.assertEqual(sanitize_ele('300.8  М'), 300.8)
        self.assertEqual(sanitize_ele('300.8米'), 300.8)

    def test_feet(self):
        self.assertEqual(sanitize_ele('100ft'), 30.48)
        self.assertEqual(sanitize_ele('200 ft'), 60.96)
        self.assertEqual(sanitize_ele('300 Feet'), 91.44)

    def test_multiple(self):
        self.assertEqual(sanitize_ele('476.2;476'),476.2)
        self.assertEqual(sanitize_ele('122;125.123 meter'),122)
        self.assertEqual(sanitize_ele('122 ; 125.123 meter'),122)

    def test_fail(self):
        with self.assertRaises(ValueError):
            sanitize_ele('hello world')
        with self.assertRaises(ValueError):
            sanitize_ele('123.123 yards')
        with self.assertRaises(ValueError):
            sanitize_ele('3km')


if __name__ == '__main__':
    unittest.main(verbosity=2)
